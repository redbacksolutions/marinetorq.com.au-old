<div class="topbar-mobile">
    <div class="active-mobile pull-left hidden-lg hidden-md">
        <button data-toggle="offcanvas" class="btn btn-offcanvas btn-toggle-canvas offcanvas" type="button">
           <i class="fa fa-bars"></i>
        </button>
    </div>
    <div class="topbar-inner pull-left">
        <div class="active-mobile pull-left" style="width: auto;">
            <a class="topbar-call-button" href="tel:1300992195"0><span class="hidden-xs hidden-sm">Call us on </span>1300 992 195</a>
        </div>
    </div>
</div>
