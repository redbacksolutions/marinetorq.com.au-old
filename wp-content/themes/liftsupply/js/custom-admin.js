(function () {
	jQuery(document).ready(function($) {

		$('body').delegate(".input_datetime", 'hover', function(e){
	            e.preventDefault();
	            $(this).datepicker({
		               defaultDate: "",
		               dateFormat: "yy-mm-dd",
		               numberOfMonths: 1,
		               showButtonPanel: true,
	            });
         });

		var hides = ['liftsupply_audio_link','liftsupply_link_link','liftsupply_link_text','liftsupply_video_link','liftsupply_gallery_files'];
		var shows = {
			audio:['liftsupply_audio_link'],
			video:['liftsupply_video_link','liftsupply_video_text'],
			link:['liftsupply_link_link'],
			gallery:['liftsupply_gallery_files']
		}
		$( '.post-type-post #post-formats-select input' ).click( function(){
			 $(hides).each( function( i, item ){
			 	$("[name="+item+']').parent().parent().hide();
			 } );
			 var s = $(this).val();
			 if( shows[s] != null ){
			 	$(shows[s]).each( function( i, is ){
			 		$("[name="+is+']').parent().parent().show();
				 } );
			 }
		} );
	});
} )( jQuery );