<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WpOpal
 * @subpackage Liftsupply
 * @since Liftsupply 1.0
 */
$_count = liftsupply_auto_count();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php liftsupply_fnc_post_thumbnail(); ?>

	<div class="blog-content">
		<header class="entry-header">
			<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && liftsupply_fnc_categorized_blog() ) : ?>

			<?php
				endif;

					the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

			?>

		</header><!-- .entry-header -->
		<div class="entry-meta">
			<?php
				if ( 'post' == get_post_type() )
					liftsupply_fnc_posted_on();

				if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
			?>
			<span class="comments-link"><span class="fa fa-comment-o"></span> <?php comments_popup_link( esc_html__( 'Leave a comment', 'liftsupply' ), esc_html__( '1 Comment', 'liftsupply' ), esc_html__( '% Comments', 'liftsupply' ) ); ?></span>
			<?php
				endif;

				edit_post_link( esc_html__( 'Edit', 'liftsupply' ), '<span class="edit-link">', '</span>' );
			?>
		</div><!-- .entry-meta -->
		<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			echo liftsupply_fnc_excerpt( 24, '...' );
		?>
		</div>
	</div><!-- .entry-content -->


</article><!-- #post-## -->
