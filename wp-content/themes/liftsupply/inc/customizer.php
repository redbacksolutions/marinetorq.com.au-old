<?php
/**
 * mode Customizer support
 *
 * @package WpOpal
 * @subpackage liftsupply
 * @since liftsupply 1.0
 */
//Update logo wordpress 4.5
if (version_compare($GLOBALS['wp_version'], '4.5', '>=')) {
    function liftsupply_fnc_setup_logo()
    {
        add_theme_support('custom-logo');
    }

    add_action('after_setup_theme', 'liftsupply_fnc_setup_logo');
}
if ( ! function_exists( 'liftsupply_fnc_customize_register' ) ) :
function liftsupply_fnc_customize_register($wp_customize){
    $wp_customize->remove_section('colors');

    // Add Panel Colors
    $wp_customize->add_panel('colors', array(
        'priority' => 15,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => esc_html__('Colors', 'liftsupply'),
    ));
    /* OpalTool: inject code */
    
    // Primary Color
    $wp_customize->add_section('liftsupply_main_color', array(
        'title'      => esc_html__('Main Color', 'liftsupply'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));
    $wp_customize->add_setting('liftsupply_maincolor_primary_color', array(
        'default'    => get_option('liftsupply_maincolor_primary_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control header_bg
    $wp_customize->add_control('liftsupply_maincolor_primary_color', array(
        'label'    => esc_html__('Primary Color', 'liftsupply'),
        'section'  => 'liftsupply_main_color',
        'type'      => 'color',
    ) );/* OpalTool: end inject code */
        // Add Section body
    $wp_customize->add_section('liftsupply_color_body', array(
        'title'      => esc_html__('Body', 'liftsupply'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting body_bg
    $wp_customize->add_setting('liftsupply_color_body_bg', array(
        'default'    => get_option('liftsupply_color_body_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control body_bg
    $wp_customize->add_control('liftsupply_color_body_bg', array(
        'label'    => esc_html__('Body Background', 'liftsupply'),
        'section'  => 'liftsupply_color_body',
        'type'      => 'color',
    ) );
    // Add setting body_color
    $wp_customize->add_setting('liftsupply_color_body_color', array(
        'default'    => get_option('liftsupply_color_body_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control body_color
    $wp_customize->add_control('liftsupply_color_body_color', array(
        'label'    => esc_html__('Body color', 'liftsupply'),
        'section'  => 'liftsupply_color_body',
        'type'      => 'color',
    ) );
    // Add Section topbar
    $wp_customize->add_section('liftsupply_color_topbar', array(
        'title'      => esc_html__('Topbar', 'liftsupply'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting topbar_bg
    $wp_customize->add_setting('liftsupply_color_topbar_bg', array(
        'default'    => get_option('liftsupply_color_topbar_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control topbar_bg
    $wp_customize->add_control('liftsupply_color_topbar_bg', array(
        'label'    => esc_html__('Topbar Background', 'liftsupply'),
        'section'  => 'liftsupply_color_topbar',
        'type'      => 'color',
    ) );
    // Add setting topbar_color
    $wp_customize->add_setting('liftsupply_color_topbar_color', array(
        'default'    => get_option('liftsupply_color_topbar_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control topbar_color
    $wp_customize->add_control('liftsupply_color_topbar_color', array(
        'label'    => esc_html__('Topbar color', 'liftsupply'),
        'section'  => 'liftsupply_color_topbar',
        'type'      => 'color',
    ) );
    // Add Section header
    $wp_customize->add_section('liftsupply_color_header', array(
        'title'      => esc_html__('Header', 'liftsupply'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting header_bg
    $wp_customize->add_setting('liftsupply_color_header_bg', array(
        'default'    => get_option('liftsupply_color_header_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control header_bg
    $wp_customize->add_control('liftsupply_color_header_bg', array(
        'label'    => esc_html__('Header Background', 'liftsupply'),
        'section'  => 'liftsupply_color_header',
        'type'      => 'color',
    ) );
    // Add setting headerv2_bg
    $wp_customize->add_setting('liftsupply_color_headerv2_bg', array(
        'default'    => get_option('liftsupply_color_headerv2_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control headerv2_bg
    $wp_customize->add_control('liftsupply_color_headerv2_bg', array(
        'label'    => esc_html__('Header V2 Background', 'liftsupply'),
        'section'  => 'liftsupply_color_header',
        'type'      => 'color',
    ) );
    // Add setting header_color
    $wp_customize->add_setting('liftsupply_color_header_color', array(
        'default'    => get_option('liftsupply_color_header_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control header_color
    $wp_customize->add_control('liftsupply_color_header_color', array(
        'label'    => esc_html__('Header color', 'liftsupply'),
        'section'  => 'liftsupply_color_header',
        'type'      => 'color',
    ) );
    // Add setting menu_color
    $wp_customize->add_setting('liftsupply_color_menu_color', array(
        'default'    => get_option('liftsupply_color_menu_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control menu_color
    $wp_customize->add_control('liftsupply_color_menu_color', array(
        'label'    => esc_html__('Megamenu color', 'liftsupply'),
        'section'  => 'liftsupply_color_header',
        'type'      => 'color',
    ) );
    // Add setting menu_color_hover
    $wp_customize->add_setting('liftsupply_color_menu_color_hover', array(
        'default'    => get_option('liftsupply_color_menu_color_hover'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control menu_color_hover
    $wp_customize->add_control('liftsupply_color_menu_color_hover', array(
        'label'    => esc_html__('Megamenu Hover color', 'liftsupply'),
        'section'  => 'liftsupply_color_header',
        'type'      => 'color',
    ) );
    // Add Section footer
    $wp_customize->add_section('liftsupply_color_footer', array(
        'title'      => esc_html__('Footer', 'liftsupply'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting footer_bg
    $wp_customize->add_setting('liftsupply_color_footer_bg', array(
        'default'    => get_option('liftsupply_color_footer_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control footer_bg
    $wp_customize->add_control('liftsupply_color_footer_bg', array(
        'label'    => esc_html__('Footer BG', 'liftsupply'),
        'section'  => 'liftsupply_color_footer',
        'type'      => 'color',
    ) );
    // Add setting footer_color
    $wp_customize->add_setting('liftsupply_color_footer_color', array(
        'default'    => get_option('liftsupply_color_footer_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control footer_color
    $wp_customize->add_control('liftsupply_color_footer_color', array(
        'label'    => esc_html__('Footer Color', 'liftsupply'),
        'section'  => 'liftsupply_color_footer',
        'type'      => 'color',
    ) );
    // Add setting heading_color
    $wp_customize->add_setting('liftsupply_color_heading_color', array(
        'default'    => get_option('liftsupply_color_heading_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control heading_color
    $wp_customize->add_control('liftsupply_color_heading_color', array(
        'label'    => esc_html__('Heading Color', 'liftsupply'),
        'section'  => 'liftsupply_color_footer',
        'type'      => 'color',
    ) );
    // Add setting newsletter_bg
    $wp_customize->add_setting('liftsupply_color_newsletter_bg', array(
        'default'    => get_option('liftsupply_color_newsletter_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control newsletter_bg
    $wp_customize->add_control('liftsupply_color_newsletter_bg', array(
        'label'    => esc_html__('Newsletter Background Color', 'liftsupply'),
        'section'  => 'liftsupply_color_footer',
        'type'      => 'color',
    ) );
    // Add setting copyright_bg
    $wp_customize->add_setting('liftsupply_color_copyright_bg', array(
        'default'    => get_option('liftsupply_color_copyright_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control copyright_bg
    $wp_customize->add_control('liftsupply_color_copyright_bg', array(
        'label'    => esc_html__('Footer Copyright Background', 'liftsupply'),
        'section'  => 'liftsupply_color_footer',
        'type'      => 'color',
    ) );
    // Add setting copyright_color
    $wp_customize->add_setting('liftsupply_color_copyright_color', array(
        'default'    => get_option('liftsupply_color_copyright_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control copyright_color
    $wp_customize->add_control('liftsupply_color_copyright_color', array(
        'label'    => esc_html__('Copyright Color', 'liftsupply'),
        'section'  => 'liftsupply_color_footer',
        'type'      => 'color',
    ) );

}
endif;
add_action('customize_register', 'liftsupply_fnc_customize_register', 99);