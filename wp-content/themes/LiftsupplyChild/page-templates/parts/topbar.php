<section id="opal-topbar" class="opal-topbar hidden">
<div class="container">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="hidden-xs hidden-sm">

                <?php
                     // WPML - Custom Languages Menu
                    liftsupply_fnc_wpml_language_buttons();
                ?>

                <?php if(has_nav_menu( 'topmenu' )): ?>

                <nav class="opal-topmenu" role="navigation">
                    <?php
                        $args = array(
                            'theme_location'  => 'topmenu',
                            'menu_class'      => 'opal-menu-top list-inline',
                            'fallback_cb'     => '',
                            'menu_id'         => 'main-topmenu'
                        );
                        wp_nav_menu($args);
                    ?>
                </nav>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</section>