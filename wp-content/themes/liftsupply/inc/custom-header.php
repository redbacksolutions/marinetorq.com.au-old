<?php
/**
 * Implement Custom Header functionality for Liftsupply
 *
 * @package WpOpal
 * @subpackage Liftsupply
 * @since Liftsupply 1.0
 */

/**
 * Set up the WordPress core custom header settings.
 *
 * @since Liftsupply 1.0
 *
 * @uses liftsupply_fnc_header_style()
 * @uses liftsupply_fnc_admin_header_style()
 * @uses liftsupply_fnc_admin_header_image()
 */
function liftsupply_fnc_custom_header_setup() {
	/**
	 * Filter Liftsupply custom-header support arguments.
	 *
	 * @since Liftsupply 1.0
	 *
	 * @param array $args {
	 *     An array of custom-header support arguments.
	 *
	 *     @type bool   $header_text            Whether to display custom header text. Default false.
	 *     @type int    $width                  Width in pixels of the custom header image. Default 1260.
	 *     @type int    $height                 Height in pixels of the custom header image. Default 240.
	 *     @type bool   $flex_height            Whether to allow flexible-height header images. Default true.
	 *     @type string $admin_head_callback    Callback function used to style the image displayed in
	 *                                          the Appearance > Header screen.
	 *     @type string $admin_preview_callback Callback function used to create the custom header markup in
	 *                                          the Appearance > Header screen.
	 * }
	 */
	add_theme_support( 'custom-header', apply_filters( 'liftsupply_fnc_custom_header_args', array(
		'default-text-color'     => 'fff',
		'width'                  => 1260,
		'height'                 => 240,
		'flex-height'            => true,
		'wp-head-callback'       => 'liftsupply_fnc_header_style',
		'admin-head-callback'    => 'liftsupply_fnc_admin_header_style',
		'admin-preview-callback' => 'liftsupply_fnc_admin_header_image',
	) ) );
}
add_action( 'after_setup_theme', 'liftsupply_fnc_custom_header_setup' );

if ( ! function_exists( 'liftsupply_fnc_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 * @see liftsupply_fnc_custom_header_setup().
 *
 */
function liftsupply_fnc_header_style() {
    ?>
    <style type="text/css" id="liftsupply-header-css">
        
        <?php
        $body_bg = get_option('liftsupply_color_body_bg');
        if( !empty($body_bg) && preg_match("#\##", $body_bg) ) : ?>
            body{
                background-color:<?php echo esc_attr($body_bg); ?>;
            }
        <?php endif; ?>

        <?php
        $body_color = get_option('liftsupply_color_body_color');
        if( !empty($body_color) && preg_match("#\##", $body_color) ) : ?>
            body{
                color:<?php echo esc_attr($body_color); ?>;
            }
        <?php endif; ?>

        <?php
        $topbar_bg = get_option('liftsupply_color_topbar_bg');
        if( !empty($topbar_bg) && preg_match("#\##", $topbar_bg) ) : ?>
            #opal-topbar{
                background-color:<?php echo esc_attr($topbar_bg); ?>;
            }
        <?php endif; ?>

        <?php
        $topbar_color = get_option('liftsupply_color_topbar_color');
        if( !empty($topbar_color) && preg_match("#\##", $topbar_color) ) : ?>
            #opal-topbar a, #opal-topbar{
                color:<?php echo esc_attr($topbar_color); ?>;
            }
        <?php endif; ?>

        <?php
        $header_bg = get_option('liftsupply_color_header_bg');
        if( !empty($header_bg) && preg_match("#\##", $header_bg) ) : ?>
            #opal-masthead .header-main{
                background-color:<?php echo esc_attr($header_bg); ?>;
            }
        <?php endif; ?>

        <?php
        $headerv2_bg = get_option('liftsupply_color_headerv2_bg');
        if( !empty($headerv2_bg) && preg_match("#\##", $headerv2_bg) ) : ?>
            .header-style-2.header-main .container{
                background-color:<?php echo esc_attr($headerv2_bg); ?>;
            }
        <?php endif; ?>

        <?php
        $header_color = get_option('liftsupply_color_header_color');
        if( !empty($header_color) && preg_match("#\##", $header_color) ) : ?>
            #opal-masthead .header-main, .opal-topcart strong, .opal-topcart .mini-cart{
                color:<?php echo esc_attr($header_color); ?>;
            }
        <?php endif; ?>

        <?php
        $menu_color = get_option('liftsupply_color_menu_color');
        if( !empty($menu_color) && preg_match("#\##", $menu_color) ) : ?>
            .navbar-mega .navbar-nav > li > a,.navbar-mega .navbar-nav > li > a .caret{
                color:<?php echo esc_attr($menu_color); ?>;
            }
        <?php endif; ?>

        <?php
        $menu_color_hover = get_option('liftsupply_color_menu_color_hover');
        if( !empty($menu_color_hover) && preg_match("#\##", $menu_color_hover) ) : ?>
            .navbar-mega .navbar-nav li.active>a, .navbar-mega .navbar-nav li.active>a .caret, .navbar-mega .navbar-nav > li > a:hover, .navbar-mega .navbar-nav > li > a:focus,.navbar-mega .navbar-nav > li > a:hover .caret, .navbar-mega .navbar-nav > li > a:focus .caret{
                color:<?php echo esc_attr($menu_color_hover); ?>;
            }
            .navbar-mega .navbar-nav > li > a:after{
                background-color:<?php echo esc_attr($menu_color_hover); ?>;
            }
        <?php endif; ?>

        <?php
        $footer_bg = get_option('liftsupply_color_footer_bg');
        if( !empty($footer_bg) && preg_match("#\##", $footer_bg) ) : ?>
            .opal-footer{
                background-color:<?php echo esc_attr($footer_bg); ?>;
            }
        <?php endif; ?>

        <?php
        $footer_color = get_option('liftsupply_color_footer_color');
        if( !empty($footer_color) && preg_match("#\##", $footer_color) ) : ?>
            .opal-footer, .opal-footer a, .opal-footer a span{
                color:<?php echo esc_attr($footer_color); ?>;
            }
        <?php endif; ?>

        <?php
        $heading_color = get_option('liftsupply_color_heading_color');
        if( !empty($heading_color) && preg_match("#\##", $heading_color) ) : ?>
            .opal-footer .widget .widget-title, .opal-footer .widget .widgettitle, .opal-footer h2, .opal-footer h3, .opal-footer h4{
                color:<?php echo esc_attr($heading_color); ?>;
            }
        <?php endif; ?>

        <?php
        $newsletter_bg = get_option('liftsupply_color_newsletter_bg');
        if( !empty($newsletter_bg) && preg_match("#\##", $newsletter_bg) ) : ?>
            .opal-footer .widget_mc4wp_form_widget .input-group .form-control{
                background-color:<?php echo esc_attr($newsletter_bg); ?>;
            }
        <?php endif; ?>

        <?php
        $copyright_bg = get_option('liftsupply_color_copyright_bg');
        if( !empty($copyright_bg) && preg_match("#\##", $copyright_bg) ) : ?>
            .opal-copyright{
                background-color:<?php echo esc_attr($copyright_bg); ?>;
            }
        <?php endif; ?>

        <?php
        $copyright_color = get_option('liftsupply_color_copyright_color');
        if( !empty($copyright_color) && preg_match("#\##", $copyright_color) ) : ?>
            .opal-copyright{
                color:<?php echo esc_attr($copyright_color); ?>;
            }
        <?php endif; ?>

    </style>
    <?php
    /* OpalTool: inject code */
    
    $liftsupply_maincolor_primary_color = get_option('liftsupply_maincolor_primary_color');
    if( (!empty($liftsupply_maincolor_primary_color) && preg_match("#\##", $liftsupply_maincolor_primary_color)) || is_customize_preview() ) : ?>
        <?php if(is_customize_preview()) $liftsupply_maincolor_primary_color = $liftsupply_maincolor_primary_color?$liftsupply_maincolor_primary_color:'__none_color__'; ?>
        <style type="text/css" id="liftsupply_maincolor_primary_color-header-css"<?php if(is_customize_preview()){ ?> data-color="<?php echo esc_attr($liftsupply_maincolor_primary_color); ?>"<?php } ?>>
            
  a:hover,a:focus ,.text-primary ,.btn-primary .badge ,.btn-link:hover,.btn-link:focus ,.nav-tabs > li.active > a,.nav-tabs > li.active > a:hover,.nav-tabs > li.active > a:focus ,.breadcrumb > .active ,.pagination > li > a:hover,.pagination > li > a:focus,.pagination > li > span:hover,.pagination > li > span:focus ,.pagination > li > a.current,.pagination > li > span.current ,a.label:hover,a.label:focus ,.panel-primary > .panel-heading .badge ,#opal-topbar a:hover ,.active-mobile .btn-search:hover .fa ,.box-group span:hover ,.opal-breadscrumb .breadcrumb li:last-child ,.opal-footer strong ,.opal-footer b ,.opal-copyright a:hover,.opal-copyright a:focus,.opal-copyright a:active ,.widget_archive a:before,.widget_recent_entries a:before,.widget_recent_comments a:first-child:before,.widget_layered_nav a:before,.widget_categories a:before ,.widget_recent_comments .comment-author-link ,.widget.widget_categories ul li a:hover,.widget.widget_layered_nav ul li a:hover,.widget.widget_layered_nav_filters ul li a:hover,.widget_nav_menu ul li a:hover,.widget.widget_pages ul li a:hover ,/* Recent Posts Widget */
.widget_recent_entries .post-date ,.widget_wpopal_recent_post .post-stick-layout .item-post.media .post-date ,.navbar-mega .navbar-nav > .open > a ,.navbar-mega .navbar-nav li.active > a ,.navbar-mega .navbar-nav li.active > a .caret ,.navbar-mega .navbar-nav li a:hover,.navbar-mega .navbar-nav li a:active,.navbar-mega .navbar-nav li a:focus ,.navbar-mega .navbar-nav li.open > a ,.navbar-mega .navbar-nav > li > a:hover,.navbar-mega .navbar-nav > li > a:focus ,.navbar-mega .navbar-nav > li > a:hover .caret,.navbar-mega .navbar-nav > li > a:focus .caret ,.navbar-mega .navbar-nav > li .dropdown-menu li a:hover ,.navbar-mega .navbar-nav > .active > a ,.widget_wpopal_menu_vertical .navbar-nav > li:focus > a,.widget_wpopal_menu_vertical .navbar-nav > li:hover > a ,.widget_wpopal_menu_vertical .navbar-nav > li:focus > a .caret:before,.widget_wpopal_menu_vertical .navbar-nav > li:hover > a .caret:before ,.widget_wpopal_menu_vertical .navbar-nav > li:focus > a:before,.widget_wpopal_menu_vertical .navbar-nav > li:hover > a:before ,.widget_wpopal_menu_vertical .navbar-nav li a:hover,.widget_wpopal_menu_vertical .navbar-nav li a:focus ,.widget_wpopal_menu_vertical .navbar-nav li .dropdown-menu .widget-title ,.widget_wpopal_menu_vertical .navbar-nav li:focus > a,.widget_wpopal_menu_vertical .navbar-nav li:hover > a ,.widget_wpopal_menu_vertical .navbar-nav li:focus > a .caret::before,.widget_wpopal_menu_vertical .navbar-nav li:hover > a .caret::before ,.widget_wpopal_menu_vertical .widget_nav_menu ul li a:hover ,.notfound-page .title ,.pagination span.current ,.entry-content .edit-link a ,article .post-content span a ,article .post-content a ,.comments .comment-author ,.comments a ,.single-portfolio .format-infomation .single-body .portfolio-info a:hover ,.ih-item .info h3:hover ,.btn-inverse-light ,.btn-outline.btn-primary ,.btn-outline.btn-success ,.btn-inverse.btn-primary:hover ,.btn-inverse.btn-success:hover ,.testimonial-collection .testimonials-name ,.pricing.pricing-v2.pricing-highlight .btn-block:hover,.pricing.pricing-v2.pricing-highlight .btn-block:focus,.pricing.pricing-v2.pricing-highlight .btn-block:active ,.pricing.pricing-v3.pricing-highlight .btn-block:hover,.pricing.pricing-v3.pricing-highlight .btn-block:focus,.pricing.pricing-v3.pricing-highlight .btn-block:active ,.pbr-team .team-body .team-job a:hover ,.blogv1 a:hover ,.feature-box .fbox-icon ,.kc_tabs.hightlight .kc_wrapper .kc_tabs_nav .ui-tabs-active ,.kc_tabs.hightlight .kc_wrapper .kc_tabs_nav .ui-tabs-active a ,.kc-pie-chart-wrapper .pie_chart_text h3 ,.btn-offcanvas:hover ,.offcanvas-showright .showright .fa ,.navbar-offcanvas ul#main-menu-offcanvas li.active > a ,.pbr-team .team-body .team-job a:hover ,.woocommerce #respond input#submit .badge,.woocommerce button.button .badge,.woocommerce input.button .badge ,.woocommerce #respond input#submit.alt .badge,.woocommerce a.button.alt .badge,.woocommerce button.button.alt .badge,.woocommerce input.button.alt .badge ,a:hover,a:focus ,.text-primary ,.btn-primary .badge ,.btn-link:hover,.btn-link:focus ,.nav-tabs > li.active > a,.nav-tabs > li.active > a:hover,.nav-tabs > li.active > a:focus ,.breadcrumb > .active ,.pagination > li > a:hover,.pagination > li > a:focus,.pagination > li > span:hover,.pagination > li > span:focus ,.pagination > li > a.current,.pagination > li > span.current ,a.label:hover,a.label:focus ,.panel-primary > .panel-heading .badge ,#opal-topbar a:hover ,.active-mobile .btn-search:hover .fa ,.box-group span:hover ,.opal-breadscrumb .breadcrumb li:last-child ,.opal-footer strong ,.opal-footer b ,.opal-copyright a:hover,.opal-copyright a:focus,.opal-copyright a:active ,.widget_archive a:before,.widget_recent_entries a:before,.widget_recent_comments a:first-child:before,.widget_layered_nav a:before,.widget_categories a:before ,.widget_recent_comments .comment-author-link ,.widget.widget_categories ul li a:hover,.widget.widget_layered_nav ul li a:hover,.widget.widget_layered_nav_filters ul li a:hover,.widget_nav_menu ul li a:hover,.widget.widget_pages ul li a:hover ,/* Recent Posts Widget */
.widget_recent_entries .post-date ,.widget_wpopal_recent_post .post-stick-layout .item-post.media .post-date ,.navbar-mega .navbar-nav > .open > a ,.navbar-mega .navbar-nav li.active > a ,.navbar-mega .navbar-nav li.active > a .caret ,.navbar-mega .navbar-nav li a:hover,.navbar-mega .navbar-nav li a:active,.navbar-mega .navbar-nav li a:focus ,.navbar-mega .navbar-nav li.open > a ,.navbar-mega .navbar-nav > li > a:hover,.navbar-mega .navbar-nav > li > a:focus ,.navbar-mega .navbar-nav > li > a:hover .caret,.navbar-mega .navbar-nav > li > a:focus .caret ,.navbar-mega .navbar-nav > li .dropdown-menu li a:hover ,.navbar-mega .navbar-nav > .active > a ,.widget_wpopal_menu_vertical .navbar-nav > li:focus > a,.widget_wpopal_menu_vertical .navbar-nav > li:hover > a ,.widget_wpopal_menu_vertical .navbar-nav > li:focus > a .caret:before,.widget_wpopal_menu_vertical .navbar-nav > li:hover > a .caret:before ,.widget_wpopal_menu_vertical .navbar-nav > li:focus > a:before,.widget_wpopal_menu_vertical .navbar-nav > li:hover > a:before ,.widget_wpopal_menu_vertical .navbar-nav li a:hover,.widget_wpopal_menu_vertical .navbar-nav li a:focus ,.widget_wpopal_menu_vertical .navbar-nav li .dropdown-menu .widget-title ,.widget_wpopal_menu_vertical .navbar-nav li:focus > a,.widget_wpopal_menu_vertical .navbar-nav li:hover > a ,.widget_wpopal_menu_vertical .navbar-nav li:focus > a .caret::before,.widget_wpopal_menu_vertical .navbar-nav li:hover > a .caret::before ,.widget_wpopal_menu_vertical .widget_nav_menu ul li a:hover ,.notfound-page .title ,.pagination span.current ,.entry-content .edit-link a ,article .post-content span a ,article .post-content a ,.comments .comment-author ,.comments a ,.single-portfolio .format-infomation .single-body .portfolio-info a:hover ,.ih-item .info h3:hover ,.btn-inverse-light ,.btn-outline.btn-primary ,.btn-outline.btn-success ,.btn-inverse.btn-primary:hover ,.btn-inverse.btn-success:hover ,.testimonial-collection .testimonials-name ,.pricing.pricing-v2.pricing-highlight .btn-block:hover,.pricing.pricing-v2.pricing-highlight .btn-block:focus,.pricing.pricing-v2.pricing-highlight .btn-block:active ,.pricing.pricing-v3.pricing-highlight .btn-block:hover,.pricing.pricing-v3.pricing-highlight .btn-block:focus,.pricing.pricing-v3.pricing-highlight .btn-block:active ,.pbr-team .team-body .team-job a:hover ,.blogv1 a:hover ,.feature-box .fbox-icon ,.kc_tabs.hightlight .kc_wrapper .kc_tabs_nav .ui-tabs-active ,.kc_tabs.hightlight .kc_wrapper .kc_tabs_nav .ui-tabs-active a ,.kc-pie-chart-wrapper .pie_chart_text h3 ,.btn-offcanvas:hover ,.offcanvas-showright .showright .fa ,.navbar-offcanvas ul#main-menu-offcanvas li.active > a ,.woocommerce ul.product_list_widget li del ,ul.product-categories li.current-cat > a,ul.product-categories li.current-cat-parent > a ,ul.product-categories li .closed:hover,ul.product-categories li .opened:hover ,ul.product-categories li a:hover ,ul.product-categories li li a:hover ,.opal-category-list #opal-accordion-categories .category-title a:hover ,.cart_list .cart-item .amount ,.cart_list .cart-main-content .remove:hover ,.opal-topcart strong ,.opal-topcart .name a:hover ,.single-product-summary .yith-wcwl-add-to-wishlist a:hover ,.single-product-summary .yith-wcwl-wishlistaddedbrowse a:hover,.single-product-summary .yith-wcwl-wishlistaddedbrowse a.add_to_wishlist:hover,.single-product-summary .yith-wcwl-add-button a:hover,.single-product-summary .yith-wcwl-add-button a.add_to_wishlist:hover,.single-product-summary .yith-wcwl-wishlistexistsbrowse a:hover,.single-product-summary .yith-wcwl-wishlistexistsbrowse a.add_to_wishlist:hover ,.single-product-summary .compare:hover ,.single-product-summary .compare:hover:before ,.single-product-summary .yith-wcwl-wishlistaddedbrowse .feedback,.single-product-summary .yith-wcwl-wishlistexistsbrowse .feedback ,.product-block .price > * ,.product-block .category a:hover ,.button-action > div.yith-compare .compare.added:hover ,.products-list .product-block .yith-wcwl-wishlistaddedbrowse a:hover,.products-list .product-block .yith-wcwl-wishlistaddedbrowse a.add_to_wishlist:hover,.products-list .product-block .yith-wcwl-add-button a:hover,.products-list .product-block .yith-wcwl-add-button a.add_to_wishlist:hover,.products-list .product-block .yith-wcwl-wishlistexistsbrowse a:hover,.products-list .product-block .yith-wcwl-wishlistexistsbrowse a.add_to_wishlist:hover,.list .product-block .yith-wcwl-wishlistaddedbrowse a:hover,.list .product-block .yith-wcwl-wishlistaddedbrowse a.add_to_wishlist:hover,.list .product-block .yith-wcwl-add-button a:hover,.list .product-block .yith-wcwl-add-button a.add_to_wishlist:hover,.list .product-block .yith-wcwl-wishlistexistsbrowse a:hover,.list .product-block .yith-wcwl-wishlistexistsbrowse a.add_to_wishlist:hover ,.products-list .product-block a.compare:hover,.products-list .product-block a.quickview:hover,.list .product-block a.compare:hover,.list .product-block a.quickview:hover ,.products-list .product-block a.compare.added,.products-list .product-block a.quickview.added,.list .product-block a.compare.added,.list .product-block a.quickview.added ,.single-product.woocommerce div.product p.price ,.woocommerce div.product .woocommerce-tabs ul.tabs li > a:hover ,.products-top-wrap .display-mode .btn.active ,.products-bottom-wrap nav.woocommerce-pagination ul span.current,.products-bottom-wrap nav.woocommerce-pagination ul li span.current ,.products-bottom-wrap nav.woocommerce-pagination ul a:hover,.products-bottom-wrap nav.woocommerce-pagination ul li a:hover ,#opal-quickview-modal .product-info p.price ,#opal-quickview-modal .close:hover .fa,#opal-quickview-modal .close:hover .icon ,.woocommerce #respond input#submit .badge,.woocommerce button.button .badge,.woocommerce input.button .badge ,.woocommerce #respond input#submit.alt .badge,.woocommerce a.button.alt .badge,.woocommerce button.button.alt .badge,.woocommerce input.button.alt .badge ,.woocommerce a.add_to_cart_button:hover:hover,.woocommerce a.product_type_external:hover:hover,#main-container .woocommerce button.button.single_add_to_cart_button:hover:hover {
                color:<?php echo esc_attr($liftsupply_maincolor_primary_color); ?>;
            }
            

.modal-content ,.owl-controls .owl-page.active span ,.modal-content ,.owl-controls .owl-page.active span ,.product-block .add-cart a.button:hover {
                border-color:<?php echo esc_attr($liftsupply_maincolor_primary_color); ?>;
            }
            
    .panel-primary > .panel-heading + .panel-collapse > .panel-body ,.navbar-mega .text-label.text-featured:after ,.panel-primary > .panel-heading + .panel-collapse > .panel-body ,.navbar-mega .text-label.text-featured:after {
                border-top-color:<?php echo esc_attr($liftsupply_maincolor_primary_color); ?>;
            }
            
  .panel-primary > .panel-footer + .panel-collapse > .panel-body ,.feature-box.feature-box-v4 ,.panel-primary > .panel-footer + .panel-collapse > .panel-body ,.feature-box.feature-box-v4 {
                border-bottom-color:<?php echo esc_attr($liftsupply_maincolor_primary_color); ?>;
            }
            

.bg-primary ,.btn-primary ,.btn-primary.disabled,.btn-primary.disabled:hover,.btn-primary.disabled:focus,.btn-primary.disabled.focus,.btn-primary.disabled:active,.btn-primary.disabled.active,.btn-primary[disabled],.btn-primary[disabled]:hover,.btn-primary[disabled]:focus,.btn-primary[disabled].focus,.btn-primary[disabled]:active,.btn-primary[disabled].active,fieldset[disabled] .btn-primary,fieldset[disabled] .btn-primary:hover,fieldset[disabled] .btn-primary:focus,fieldset[disabled] .btn-primary.focus,fieldset[disabled] .btn-primary:active,fieldset[disabled] .btn-primary.active ,.dropdown-menu > .active > a,.dropdown-menu > .active > a:hover,.dropdown-menu > .active > a:focus ,.nav-pills > li.active > a,.nav-pills > li.active > a:hover,.nav-pills > li.active > a:focus ,.pagination > .active > a,.pagination > .active > a:hover,.pagination > .active > a:focus,.pagination > .active > span,.pagination > .active > span:hover,.pagination > .active > span:focus ,.label-primary ,.progress-bar ,.list-group-item.active,.list-group-item.active:hover,.list-group-item.active:focus ,.panel-primary > .panel-heading ,.topbar-mobile ,.active-mobile .input-group-btn ,.home2 .product-block span.sale-off,.home2 .product-block span.onsale ,.widget_calendar tbody a ,.widget_calendar #today ,.navbar-mega .navbar-toggle .icon-bar ,.navbar-mega .navbar-nav > li > a:after ,.navbar-mega .text-label.text-featured ,.navbar-mega-theme ,.ih-item.square.effect16 .info ,.ih-item.square.effect16 .info ,.bg-primary ,.btn-outline-light:hover,.btn-outline-light:focus,.btn-outline-light:active,.btn-outline-light.active ,.open .btn-outline-light.dropdown-toggle ,.btn-outline-dark:hover,.btn-outline-dark:focus,.btn-outline-dark:active,.btn-outline-dark.active ,.open .btn-outline-dark.dropdown-toggle ,.btn-inverse-light .badge ,.owl-controls .owl-page span ,.dropcap.dropcap-v2 ,.pricing.pricing-v2 .plan-price ,.pricing.pricing-v2 .plan-price .plan-price-body ,.pricing.pricing-v2.pricing-highlight .plan-price-body ,.pricing.pricing-v2.pricing-highlight .btn-block ,.pricing.pricing-v3.pricing-highlight .plan-title ,.pricing.pricing-v3.pricing-highlight .plan-price ,.pricing.pricing-v3.pricing-highlight .btn-block ,.pricing.pricing-highlight .plan-title ,.pbr-team.other-team ,.feature-box-v3 .fbox-icon i ,h2.kc_title:after ,.home2 .product-block span.sale-off,.home2 .product-block span.onsale ,/**
 * Checkout Page
 */
/**
 * Thanks you page
 */
/************************
 * Form Styles
 ************************/
.woocommerce #respond input#submit,.woocommerce button.button,.woocommerce input.button ,.woocommerce #respond input#submit.disabled,.woocommerce #respond input#submit.disabled:hover,.woocommerce #respond input#submit.disabled:focus,.woocommerce #respond input#submit.disabled.focus,.woocommerce #respond input#submit.disabled:active,.woocommerce #respond input#submit.disabled.active,.woocommerce #respond input#submit[disabled],.woocommerce #respond input#submit[disabled]:hover,.woocommerce #respond input#submit[disabled]:focus,.woocommerce #respond input#submit[disabled].focus,.woocommerce #respond input#submit[disabled]:active,.woocommerce #respond input#submit[disabled].active,fieldset[disabled] .woocommerce #respond input#submit,fieldset[disabled] .woocommerce #respond input#submit:hover,fieldset[disabled] .woocommerce #respond input#submit:focus,fieldset[disabled] .woocommerce #respond input#submit.focus,fieldset[disabled] .woocommerce #respond input#submit:active,fieldset[disabled] .woocommerce #respond input#submit.active,.woocommerce button.button.disabled,.woocommerce button.button.disabled:hover,.woocommerce button.button.disabled:focus,.woocommerce button.button.disabled.focus,.woocommerce button.button.disabled:active,.woocommerce button.button.disabled.active,.woocommerce button.button[disabled],.woocommerce button.button[disabled]:hover,.woocommerce button.button[disabled]:focus,.woocommerce button.button[disabled].focus,.woocommerce button.button[disabled]:active,.woocommerce button.button[disabled].active,fieldset[disabled] .woocommerce button.button,fieldset[disabled] .woocommerce button.button:hover,fieldset[disabled] .woocommerce button.button:focus,fieldset[disabled] .woocommerce button.button.focus,fieldset[disabled] .woocommerce button.button:active,fieldset[disabled] .woocommerce button.button.active,.woocommerce input.button.disabled,.woocommerce input.button.disabled:hover,.woocommerce input.button.disabled:focus,.woocommerce input.button.disabled.focus,.woocommerce input.button.disabled:active,.woocommerce input.button.disabled.active,.woocommerce input.button[disabled],.woocommerce input.button[disabled]:hover,.woocommerce input.button[disabled]:focus,.woocommerce input.button[disabled].focus,.woocommerce input.button[disabled]:active,.woocommerce input.button[disabled].active,fieldset[disabled] .woocommerce input.button,fieldset[disabled] .woocommerce input.button:hover,fieldset[disabled] .woocommerce input.button:focus,fieldset[disabled] .woocommerce input.button.focus,fieldset[disabled] .woocommerce input.button:active,fieldset[disabled] .woocommerce input.button.active ,.woocommerce #respond input#submit.alt,.woocommerce a.button.alt,.woocommerce button.button.alt,.woocommerce input.button.alt ,.woocommerce #respond input#submit.alt.disabled,.woocommerce #respond input#submit.alt.disabled:hover,.woocommerce #respond input#submit.alt.disabled:focus,.woocommerce #respond input#submit.alt.disabled.focus,.woocommerce #respond input#submit.alt.disabled:active,.woocommerce #respond input#submit.alt.disabled.active,.woocommerce #respond input#submit.alt[disabled],.woocommerce #respond input#submit.alt[disabled]:hover,.woocommerce #respond input#submit.alt[disabled]:focus,.woocommerce #respond input#submit.alt[disabled].focus,.woocommerce #respond input#submit.alt[disabled]:active,.woocommerce #respond input#submit.alt[disabled].active,fieldset[disabled] .woocommerce #respond input#submit.alt,fieldset[disabled] .woocommerce #respond input#submit.alt:hover,fieldset[disabled] .woocommerce #respond input#submit.alt:focus,fieldset[disabled] .woocommerce #respond input#submit.alt.focus,fieldset[disabled] .woocommerce #respond input#submit.alt:active,fieldset[disabled] .woocommerce #respond input#submit.alt.active,.woocommerce a.button.alt.disabled,.woocommerce a.button.alt.disabled:hover,.woocommerce a.button.alt.disabled:focus,.woocommerce a.button.alt.disabled.focus,.woocommerce a.button.alt.disabled:active,.woocommerce a.button.alt.disabled.active,.woocommerce a.button.alt[disabled],.woocommerce a.button.alt[disabled]:hover,.woocommerce a.button.alt[disabled]:focus,.woocommerce a.button.alt[disabled].focus,.woocommerce a.button.alt[disabled]:active,.woocommerce a.button.alt[disabled].active,fieldset[disabled] .woocommerce a.button.alt,fieldset[disabled] .woocommerce a.button.alt:hover,fieldset[disabled] .woocommerce a.button.alt:focus,fieldset[disabled] .woocommerce a.button.alt.focus,fieldset[disabled] .woocommerce a.button.alt:active,fieldset[disabled] .woocommerce a.button.alt.active,.woocommerce button.button.alt.disabled,.woocommerce button.button.alt.disabled:hover,.woocommerce button.button.alt.disabled:focus,.woocommerce button.button.alt.disabled.focus,.woocommerce button.button.alt.disabled:active,.woocommerce button.button.alt.disabled.active,.woocommerce button.button.alt[disabled],.woocommerce button.button.alt[disabled]:hover,.woocommerce button.button.alt[disabled]:focus,.woocommerce button.button.alt[disabled].focus,.woocommerce button.button.alt[disabled]:active,.woocommerce button.button.alt[disabled].active,fieldset[disabled] .woocommerce button.button.alt,fieldset[disabled] .woocommerce button.button.alt:hover,fieldset[disabled] .woocommerce button.button.alt:focus,fieldset[disabled] .woocommerce button.button.alt.focus,fieldset[disabled] .woocommerce button.button.alt:active,fieldset[disabled] .woocommerce button.button.alt.active,.woocommerce input.button.alt.disabled,.woocommerce input.button.alt.disabled:hover,.woocommerce input.button.alt.disabled:focus,.woocommerce input.button.alt.disabled.focus,.woocommerce input.button.alt.disabled:active,.woocommerce input.button.alt.disabled.active,.woocommerce input.button.alt[disabled],.woocommerce input.button.alt[disabled]:hover,.woocommerce input.button.alt[disabled]:focus,.woocommerce input.button.alt[disabled].focus,.woocommerce input.button.alt[disabled]:active,.woocommerce input.button.alt[disabled].active,fieldset[disabled] .woocommerce input.button.alt,fieldset[disabled] .woocommerce input.button.alt:hover,fieldset[disabled] .woocommerce input.button.alt:focus,fieldset[disabled] .woocommerce input.button.alt.focus,fieldset[disabled] .woocommerce input.button.alt:active,fieldset[disabled] .woocommerce input.button.alt.active ,.bg-primary ,.btn-primary ,.btn-primary.disabled,.btn-primary.disabled:hover,.btn-primary.disabled:focus,.btn-primary.disabled.focus,.btn-primary.disabled:active,.btn-primary.disabled.active,.btn-primary[disabled],.btn-primary[disabled]:hover,.btn-primary[disabled]:focus,.btn-primary[disabled].focus,.btn-primary[disabled]:active,.btn-primary[disabled].active,fieldset[disabled] .btn-primary,fieldset[disabled] .btn-primary:hover,fieldset[disabled] .btn-primary:focus,fieldset[disabled] .btn-primary.focus,fieldset[disabled] .btn-primary:active,fieldset[disabled] .btn-primary.active ,.dropdown-menu > .active > a,.dropdown-menu > .active > a:hover,.dropdown-menu > .active > a:focus ,.nav-pills > li.active > a,.nav-pills > li.active > a:hover,.nav-pills > li.active > a:focus ,.pagination > .active > a,.pagination > .active > a:hover,.pagination > .active > a:focus,.pagination > .active > span,.pagination > .active > span:hover,.pagination > .active > span:focus ,.label-primary ,.progress-bar ,.list-group-item.active,.list-group-item.active:hover,.list-group-item.active:focus ,.panel-primary > .panel-heading ,.topbar-mobile ,.active-mobile .input-group-btn ,.home2 .product-block span.sale-off,.home2 .product-block span.onsale ,.widget_calendar tbody a ,.widget_calendar #today ,.navbar-mega .navbar-toggle .icon-bar ,.navbar-mega .navbar-nav > li > a:after ,.navbar-mega .text-label.text-featured ,.navbar-mega-theme ,.ih-item.square.effect16 .info ,.ih-item.square.effect16 .info ,.bg-primary ,.btn-outline-light:hover,.btn-outline-light:focus,.btn-outline-light:active,.btn-outline-light.active ,.open .btn-outline-light.dropdown-toggle ,.btn-outline-dark:hover,.btn-outline-dark:focus,.btn-outline-dark:active,.btn-outline-dark.active ,.open .btn-outline-dark.dropdown-toggle ,.btn-inverse-light .badge ,.owl-controls .owl-page span ,.dropcap.dropcap-v2 ,.pricing.pricing-v2 .plan-price ,.pricing.pricing-v2 .plan-price .plan-price-body ,.pricing.pricing-v2.pricing-highlight .plan-price-body ,.pricing.pricing-v2.pricing-highlight .btn-block ,.pricing.pricing-v3.pricing-highlight .plan-title ,.pricing.pricing-v3.pricing-highlight .plan-price ,.pricing.pricing-v3.pricing-highlight .btn-block ,.pricing.pricing-highlight .plan-title ,.pbr-team.other-team ,.feature-box-v3 .fbox-icon i ,h2.kc_title:after ,.widget_price_filter .ui-slider .ui-slider-handle ,/**
 * Widget Products Categories Menu 
 */
.widget_product_categories .widget-title,.widget_categories .widget-title ,.opal-topcart .cart-icon ,.product-info .product-topinfo .product-nav a:hover ,.product-block .add-cart a.button ,.button-action > div.yith-wcwl-add-to-wishlist a.add_to_wishlist,.button-action > div.yith-wcwl-add-to-wishlist a.compare,.button-action > div.yith-wcwl-add-to-wishlist a.quickview,.button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a,.button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a,.button-action > div.yith-compare a.add_to_wishlist,.button-action > div.yith-compare a.compare,.button-action > div.yith-compare a.quickview,.button-action > div.yith-compare .yith-wcwl-wishlistaddedbrowse a,.button-action > div.yith-compare .yith-wcwl-wishlistexistsbrowse a,.button-action > div.quick-view a.add_to_wishlist,.button-action > div.quick-view a.compare,.button-action > div.quick-view a.quickview,.button-action > div.quick-view .yith-wcwl-wishlistaddedbrowse a,.button-action > div.quick-view .yith-wcwl-wishlistexistsbrowse a ,.products-list .product-block .add-cart a.button,.list .product-block .add-cart a.button ,.single-product.woocommerce div.product form.cart .button ,.woocommerce div.product .woocommerce-tabs ul.tabs:after ,#opal-quickview-modal .product-info a.add_to_cart_button ,#opal-quickview-modal .carousel-indicators .active ,/**
 * Checkout Page
 */
/**
 * Thanks you page
 */
/************************
 * Form Styles
 ************************/
.woocommerce #respond input#submit,.woocommerce button.button,.woocommerce input.button ,.woocommerce #respond input#submit.disabled,.woocommerce #respond input#submit.disabled:hover,.woocommerce #respond input#submit.disabled:focus,.woocommerce #respond input#submit.disabled.focus,.woocommerce #respond input#submit.disabled:active,.woocommerce #respond input#submit.disabled.active,.woocommerce #respond input#submit[disabled],.woocommerce #respond input#submit[disabled]:hover,.woocommerce #respond input#submit[disabled]:focus,.woocommerce #respond input#submit[disabled].focus,.woocommerce #respond input#submit[disabled]:active,.woocommerce #respond input#submit[disabled].active,fieldset[disabled] .woocommerce #respond input#submit,fieldset[disabled] .woocommerce #respond input#submit:hover,fieldset[disabled] .woocommerce #respond input#submit:focus,fieldset[disabled] .woocommerce #respond input#submit.focus,fieldset[disabled] .woocommerce #respond input#submit:active,fieldset[disabled] .woocommerce #respond input#submit.active,.woocommerce button.button.disabled,.woocommerce button.button.disabled:hover,.woocommerce button.button.disabled:focus,.woocommerce button.button.disabled.focus,.woocommerce button.button.disabled:active,.woocommerce button.button.disabled.active,.woocommerce button.button[disabled],.woocommerce button.button[disabled]:hover,.woocommerce button.button[disabled]:focus,.woocommerce button.button[disabled].focus,.woocommerce button.button[disabled]:active,.woocommerce button.button[disabled].active,fieldset[disabled] .woocommerce button.button,fieldset[disabled] .woocommerce button.button:hover,fieldset[disabled] .woocommerce button.button:focus,fieldset[disabled] .woocommerce button.button.focus,fieldset[disabled] .woocommerce button.button:active,fieldset[disabled] .woocommerce button.button.active,.woocommerce input.button.disabled,.woocommerce input.button.disabled:hover,.woocommerce input.button.disabled:focus,.woocommerce input.button.disabled.focus,.woocommerce input.button.disabled:active,.woocommerce input.button.disabled.active,.woocommerce input.button[disabled],.woocommerce input.button[disabled]:hover,.woocommerce input.button[disabled]:focus,.woocommerce input.button[disabled].focus,.woocommerce input.button[disabled]:active,.woocommerce input.button[disabled].active,fieldset[disabled] .woocommerce input.button,fieldset[disabled] .woocommerce input.button:hover,fieldset[disabled] .woocommerce input.button:focus,fieldset[disabled] .woocommerce input.button.focus,fieldset[disabled] .woocommerce input.button:active,fieldset[disabled] .woocommerce input.button.active ,.woocommerce #respond input#submit.alt,.woocommerce a.button.alt,.woocommerce button.button.alt,.woocommerce input.button.alt ,.woocommerce #respond input#submit.alt.disabled,.woocommerce #respond input#submit.alt.disabled:hover,.woocommerce #respond input#submit.alt.disabled:focus,.woocommerce #respond input#submit.alt.disabled.focus,.woocommerce #respond input#submit.alt.disabled:active,.woocommerce #respond input#submit.alt.disabled.active,.woocommerce #respond input#submit.alt[disabled],.woocommerce #respond input#submit.alt[disabled]:hover,.woocommerce #respond input#submit.alt[disabled]:focus,.woocommerce #respond input#submit.alt[disabled].focus,.woocommerce #respond input#submit.alt[disabled]:active,.woocommerce #respond input#submit.alt[disabled].active,fieldset[disabled] .woocommerce #respond input#submit.alt,fieldset[disabled] .woocommerce #respond input#submit.alt:hover,fieldset[disabled] .woocommerce #respond input#submit.alt:focus,fieldset[disabled] .woocommerce #respond input#submit.alt.focus,fieldset[disabled] .woocommerce #respond input#submit.alt:active,fieldset[disabled] .woocommerce #respond input#submit.alt.active,.woocommerce a.button.alt.disabled,.woocommerce a.button.alt.disabled:hover,.woocommerce a.button.alt.disabled:focus,.woocommerce a.button.alt.disabled.focus,.woocommerce a.button.alt.disabled:active,.woocommerce a.button.alt.disabled.active,.woocommerce a.button.alt[disabled],.woocommerce a.button.alt[disabled]:hover,.woocommerce a.button.alt[disabled]:focus,.woocommerce a.button.alt[disabled].focus,.woocommerce a.button.alt[disabled]:active,.woocommerce a.button.alt[disabled].active,fieldset[disabled] .woocommerce a.button.alt,fieldset[disabled] .woocommerce a.button.alt:hover,fieldset[disabled] .woocommerce a.button.alt:focus,fieldset[disabled] .woocommerce a.button.alt.focus,fieldset[disabled] .woocommerce a.button.alt:active,fieldset[disabled] .woocommerce a.button.alt.active,.woocommerce button.button.alt.disabled,.woocommerce button.button.alt.disabled:hover,.woocommerce button.button.alt.disabled:focus,.woocommerce button.button.alt.disabled.focus,.woocommerce button.button.alt.disabled:active,.woocommerce button.button.alt.disabled.active,.woocommerce button.button.alt[disabled],.woocommerce button.button.alt[disabled]:hover,.woocommerce button.button.alt[disabled]:focus,.woocommerce button.button.alt[disabled].focus,.woocommerce button.button.alt[disabled]:active,.woocommerce button.button.alt[disabled].active,fieldset[disabled] .woocommerce button.button.alt,fieldset[disabled] .woocommerce button.button.alt:hover,fieldset[disabled] .woocommerce button.button.alt:focus,fieldset[disabled] .woocommerce button.button.alt.focus,fieldset[disabled] .woocommerce button.button.alt:active,fieldset[disabled] .woocommerce button.button.alt.active,.woocommerce input.button.alt.disabled,.woocommerce input.button.alt.disabled:hover,.woocommerce input.button.alt.disabled:focus,.woocommerce input.button.alt.disabled.focus,.woocommerce input.button.alt.disabled:active,.woocommerce input.button.alt.disabled.active,.woocommerce input.button.alt[disabled],.woocommerce input.button.alt[disabled]:hover,.woocommerce input.button.alt[disabled]:focus,.woocommerce input.button.alt[disabled].focus,.woocommerce input.button.alt[disabled]:active,.woocommerce input.button.alt[disabled].active,fieldset[disabled] .woocommerce input.button.alt,fieldset[disabled] .woocommerce input.button.alt:hover,fieldset[disabled] .woocommerce input.button.alt:focus,fieldset[disabled] .woocommerce input.button.alt.focus,fieldset[disabled] .woocommerce input.button.alt:active,fieldset[disabled] .woocommerce input.button.alt.active ,.woocommerce .wishlist_table td.product-add-to-cart a {
                background-color:<?php echo esc_attr($liftsupply_maincolor_primary_color); ?>;
            }

    </style>
    <?php
    endif;/* OpalTool: end inject code */
}
endif; // liftsupply_fnc_header_style


if ( ! function_exists( 'liftsupply_fnc_admin_header_style' ) ) :
/**
 * Style the header image displayed on the Appearance > Header screen.
 *
 * @see liftsupply_fnc_custom_header_setup()
 *
 * @since Liftsupply 1.0
 */
function liftsupply_fnc_admin_header_style() {
?>
	<style type="text/css" id="liftsupply-admin-header-css">
	.appearance_page_custom-header #headimg {
		background-color: #000;
		border: none;
		max-width: 1260px;
		min-height: 48px;
	}
	#headimg h1 {
		font-family: Lato, sans-serif;
		font-size: 18px;
		line-height: 48px;
		margin: 0 0 0 30px;
	}
	.rtl #headimg h1  {
		margin: 0 30px 0 0;
	}
	#headimg h1 a {
		color: #fff;
		text-decoration: none;
	}
	#headimg img {
		vertical-align: middle;
	}

<?php
}
endif; // liftsupply_fnc_admin_header_style

if ( ! function_exists( 'liftsupply_fnc_admin_header_image' ) ) :
/**
 * Create the custom header image markup displayed on the Appearance > Header screen.
 *
 * @see liftsupply_fnc_custom_header_setup()
 *
 * @since Liftsupply 1.0
 */
function liftsupply_fnc_admin_header_image() {
?>
	<div id="headimg">
		<?php if ( get_header_image() ) : ?>
		<img src="<?php header_image(); ?>" alt="">
		<?php endif; ?>
		<h1 class="displaying-header-text"><a id="name" style="<?php echo esc_attr( sprintf( 'color: #%s;', get_header_textcolor() ) ); ?>" onclick="return false;" href="<?php echo esc_url( home_url( '/' ) ); ?>" tabindex="-1"><?php bloginfo( 'name' ); ?></a></h1>
	</div>
<?php
}
endif; // liftsupply_fnc_admin_header_image