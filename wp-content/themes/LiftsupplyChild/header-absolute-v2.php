<?php
/**
 * The Header for our theme: Main Darker Background. Logo left + Main menu and Right sidebar. Below Category Search + Mini Shopping basket.
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WpOpal
 * @subpackage Liftsupply
 * @since Liftsupply 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NVP5CZZ');</script>
	<!-- End Google Tag Manager -->

	<meta name="google-site-verification" content="1zgNGQZkxvK5mEvx65v1yO1PktV6ndGTtZSIA8Gxc6I" />
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVP5CZZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site"><div class="opal-page-inner row-offcanvas row-offcanvas-left">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header" class="hidden-xs hidden-sm">
		<a href="<?php echo esc_url( get_option('header_image_link','#') ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>
	<?php get_template_part( 'page-templates/parts/topbar', 'mobile' ); ?>
	<header id="opal-masthead" class="site-header header-absolute <?php  if( liftsupply_fnc_theme_options('keepheader') ) : ?> keep-header<?php endif; ?>" role="banner">

	<?php get_template_part( 'page-templates/parts/topbar'); ?>

	<div class="header-main header-style-2">
		<div class="container">
			<div class="row">
				<div class="logo-wrapper col-md-4 col-lg-4">
		 			<?php get_template_part( 'page-templates/parts/logo' ); ?>
				</div>
				<section id="opal-mainmenu" class="opal-mainmenu">
					<div class="col-lg-8 col-md-8 col-xs-12 pull-right">
						<div class="inner pull-left"><?php get_template_part( 'page-templates/parts/nav' ); ?></div>
						<div class="opal-header-right pull-right hidden-xs hidden-sm">
							<div class="header-inner">
								<div class="pull-right">
									<?php do_action( "liftsupply_template_header_right" ); ?>
								</div>
								<div id="search-container" class="search-box-wrapper pull-right">
									<div class="opal-dropdow-search dropdown">
									  	<a data-target=".bs-search-modal-lg" data-toggle="modal" class="search-focus btn dropdown-toggle dropdown-toggle-overlay">
							                <i class="fa fa-search"></i>
							            </a>
							            <div class="modal fade bs-search-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
							              <div class="modal-dialog modal-lg">
							                <div class="modal-content">
							                    <div class="modal-header">
							                      <button aria-label="Close" data-dismiss="modal" class="close btn btn-sm btn-primary pull-right" type="button"><span aria-hidden="true">x</span></button>
							                      <h4 id="gridSystemModalLabel" class="modal-title"><?php esc_html_e( 'Search', 'liftsupply' ); ?></h4>
							                    </div>
							                    <div class="modal-body">
							                      <?php get_template_part( 'page-templates/parts/search-overlay' ); ?>
							                    </div>
							                </div>
							              </div>
							            </div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</section>
			</div>
		</div>
	</div>
	</header><!-- #masthead -->

	<?php do_action( 'liftsupply_template_header_after' ); ?>

	<section id="main" class="site-main">
