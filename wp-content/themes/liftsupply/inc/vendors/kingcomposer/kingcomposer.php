<?php
	add_action('init', 'liftsupply_element_kingcomposer_map', 99 );

	add_filter( 'kc_autocomplete_product_ids', 'liftsupply_autocomplete_product_ids' );

	function liftsupply_autocomplete_product_ids( $data ){
	 
	   if( isset($_POST['s']) ){
 			$output = array();
			$vc_taxonomies_types = array('product_cat');
			$search_string = trim( $_POST['s']);  
		 	
		 	$args = array(
		 		's' => $search_string,
		 		'post_type' => 'product'
		 	);
		 	$products = get_posts( $args );

			if ( is_array( $products ) && ! empty( $products ) ) {

				foreach ( $products as $t ) {
					if ( is_object( $t ) ) {

						$output[] = $t->post_name.':'.$t->post_title;
					}
				}
			}

			wp_reset_postdata();
	 		return array( "Products" =>  $output );
	 	}
	}

	function liftsupply_element_kingcomposer_map(){
		global $kc;

		$maps = array();
		// Degree-360
 		$maps['element_degree_360'] =  array(
				"name"        => esc_html__("Degree-360", 'liftsupply'),
				"class"       => "",
				"description" => 'Show Degree-360',
				"category"    => esc_html__('Elements', 'liftsupply'),
				"icon"        => 'kc-icon-gallery',
				"params"      => array(

				array(
					'type'           => 'autocomplete',
					'label'          => esc_html__( 'Select Product', 'liftsupply' ),
					'name'           => 'product_ids',
					'description'    => esc_html__( 'Select Product to display', 'liftsupply' ),
					'admin_label'    => true,
					'options' => array(
						'post_type'     => 'product',
					),
					'description' => esc_html__( 'Only get a product for show.', 'liftsupply' ),
                ),

                array(
					'name' => 'flag',
					'label' => esc_html__( 'Show Images by product', 'liftsupply' ),
					'type' => 'checkbox',
					'options' => array(
						'yes' =>  esc_html__( 'Yes, Please!', 'liftsupply' ),
					),
					'description' => esc_html__( 'Are you sure use Images of Product ?', 'liftsupply' )
				),

		    	array(
					"type"        => "textfield",
					"label"       => esc_html__("Title", 'liftsupply'),
					"name"        => "title",
					"value"       => '',
					"admin_label" => true
				),
				array(
					"type"        => "textfield",
					"label"       => esc_html__("Prefix Class", 'liftsupply'),
					"name"        => "class",
					"value"       => '',
					"admin_label" => true
				),
				array(
					"type"        => "attach_images",
					"label"       => esc_html__("Images", 'liftsupply'),
					"name"        => "images",
					'description' => esc_html__( 'Upload multiple image to the carousel with the SHIFT key holding.', 'liftsupply' ),
					'admin_label' => true
				),
			  )
		);
		$maps = apply_filters( 'liftsupply_element_kingcomposer_map', $maps );
	    $kc->add_map( $maps );
	} // end class
?>