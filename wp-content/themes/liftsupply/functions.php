<?php
/**
 * Liftsupply functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link https://codex.wordpress.org/Plugin_API
 *
 * @package WpOpal
 * @subpackage Liftsupply
 * @since Liftsupply 1.0
 */
define( 'LIFTSUPPLY_THEME_VERSION', '1.0' );

/**
 * Set up the content width value based on the theme's design.
 *
 * @see liftsupply_fnc_content_width()
 *
 * @since Liftsupply 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

function liftsupply_fnc_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'liftsupply_fnc_content_width', 810 );
}
add_action( 'after_setup_theme', 'liftsupply_fnc_content_width', 0 );



if ( ! function_exists( 'liftsupply_fnc_setup' ) ) :
/**
 * Liftsupply setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Liftsupply 1.0
 */
function liftsupply_fnc_setup() {

	/*
	 * Make Liftsupply available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Liftsupply, use a find and
	 * replace to change 'liftsupply' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'liftsupply', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.

	add_editor_style( 'css/editor-style.css' );
	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => esc_html__( 'Main menu', 'liftsupply' ),
		'secondary' => esc_html__( 'Menu in left sidebar', 'liftsupply' ),
		'topmenu'	=> esc_html__( 'Topbar Menu', 'liftsupply' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'liftsupply_fnc_custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );

	// add support for display browser title
	add_theme_support( 'title-tag' );
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
endif; // liftsupply_fnc_setup
add_action( 'after_setup_theme', 'liftsupply_fnc_setup' );

/**
 * batch including all files in a path.
 *
 * @param String $path : PATH_DIR/*.php or PATH_DIR with $ifiles not empty
 */
function liftsupply_fnc_includes( $path, $ifiles=array() ){

    if( !empty($ifiles) ){
         foreach( $ifiles as $key => $file ){
            $file  = $path.'/'.$file;
            if(is_file($file)){
                require($file);
            }
         }
    }else {
        $files = glob($path);
        foreach ($files as $key => $file) {
            if(is_file($file)){
                require($file);
            }
        }
    }
}

/**
 * get theme prefix which will use for own theme setting as page config, customizer
 *
 * @return string text_domain
 */
function liftsupply_themer_get_theme_prefix(){
	return 'liftsupply_';
}

add_filter( 'wpopal_themer_get_theme_prefix', 'liftsupply_themer_get_theme_prefix' );

/**
 * Get Theme Option Value.
 * @param String $name : name of prameters
 */
function liftsupply_fnc_theme_options($name, $default = false) {

    // get the meta from the database
    $options = ( get_option( 'wpopal_theme_options' ) ) ? get_option( 'wpopal_theme_options' ) : null;

    // return the option if it exists
    if ( isset( $options[$name] ) ) {
        return apply_filters( 'wpopal_theme_options_$name', $options[ $name ] );
    }
    if( get_option( $name ) ){
        return get_option( $name );
    }
    // return default if nothing else
    return apply_filters( 'wpopal_theme_options_$name', $default );
}


/**
 * Function for remove srcset (WP4.4)
 *
 */
function liftsupply_fnc_disable_srcset( $sources ) {
    return false;
}
add_filter( 'wp_calculate_image_srcset', 'liftsupply_fnc_disable_srcset' );

 
/**
 * Get plugin icon
 */
function wpopalbootstrap_get_plugin_icon_image($slug)
{

    switch ($slug) {
        case 'revslider':
            $img = get_template_directory_uri() . '/assets/plugins/logo-rv.png';
            break;
        case 'yith-woocommerce-compare':
        case 'yith-woocommerce-wishlist':
        case 'yith-woocommerce-quick-view' :
            $img = 'https://ps.w.org/' . $slug . '/assets/icon-128x128.jpg';
            break;
        default:
            $img = 'https://ps.w.org/' . $slug . '/assets/icon-128x128.png';
            break;
    }

    return '<img src="' . $img . '"/>';
}


/**
 * Require function for including 3rd plugins
 *
 */

if (is_admin()) {
    require get_template_directory() . '/inc/admin/class-menu.php';
    /**
     * Load include plugins using for this project
     */
    require get_template_directory() . '/inc/tgm/class-tgm-plugin-activation.php';


    add_action('tgmpa_register', 'liftsupply_fnc_get_load_plugins');
	function liftsupply_fnc_get_load_plugins(){

		$plugins[] =(array(
			'name'                     => esc_html__('MetaBox', 'liftsupply'),// The plugin name
		    'slug'                     => 'meta-box', // The plugin slug (typically the folder name)
		    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
		));

		$plugins[] =(array(
			'name'                     => esc_html__('WooCommerce','liftsupply'), // The plugin name
		    'slug'                     => 'woocommerce', // The plugin slug (typically the folder name)
		    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
		));


		$plugins[] =(array(
			'name'                     => esc_html__('MailChimp', 'liftsupply'),// The plugin name
		    'slug'                     => 'mailchimp-for-wp', // The plugin slug (typically the folder name)
		    'required'                 =>  true
		));

		$plugins[] =(array(
			'name'                     => esc_html__('Contact Form 7','liftsupply'), // The plugin name
		    'slug'                     => 'contact-form-7', // The plugin slug (typically the folder name)
		    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
		));

		$plugins[] =(array(
			'name'                     => esc_html__('King Composer - Page Builder', 'liftsupply'),// The plugin name
		    'slug'                     => 'kingcomposer', // The plugin slug (typically the folder name)
		    'required'                 => true,
		    
		));

		$plugins[] =(array(
			'name'                     => esc_html__('Revolution Slider', 'liftsupply'),// The plugin name
	        'slug'                     => 'revslider', // The plugin slug (typically the folder name)
	        'required'                 => true ,
	        'source'                   => esc_url( 'http://source.wpopal.com/plugins/revslider.zip' ), // The plugin source
		));

		$plugins[] =(array(
			'name'                     => esc_html__('Wpopal Themer For Themes', 'liftsupply'),// The plugin name
	        'slug'                     => 'wpopal-themer', // The plugin slug (typically the folder name)
	        'required'                 => true ,
	        'source'				   => esc_url( 'http://source.wpopal.com/plugins/wpopal-themer.zip' )
		));

		$plugins[] =(array(
			'name'                     => esc_html__('YITH WooCommerce Wishlist', 'liftsupply'),// The plugin name
		    'slug'                     => 'yith-woocommerce-wishlist', // The plugin slug (typically the folder name)
		    'required'                 =>  true
		));

	   /*
		 * Array of configuration settings. Amend each line as needed.
		 *
		 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
		 * strings available, please help us make TGMPA even better by giving us access to these translations or by
		 * sending in a pull-request with .po file(s) with the translations.
		 *
		 * Only uncomment the strings in the config array if you want to customize the strings.
		 */
	    $config = array(
	        'id'           => 'wpopalbootstrap-required',  // Unique ID for hashing notices for multiple instances of TGMPA.
	        'default_path' => '',                      // Default absolute path to bundled plugins.
	        'menu'         => 'tgmpa-install-plugins', // Menu slug.
	        'has_notices'  => true,                    // Show admin notices or not.
	        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
	        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
	        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
	        'message'      => '',                      // Message to output right before the plugins table.
	    );

	    tgmpa( $plugins, $config );
}
}
/**
 * Register three Liftsupply widget areas.
 *
 * @since Liftsupply 1.0
 */
function liftsupply_fnc_registart_widgets_sidebars() {

	register_sidebar(
	array(
		'name'          => esc_html__( 'Sidebar Default', 'liftsupply' ),
		'id'            => 'sidebar-default',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Newsletter' , 'liftsupply'),
		'id'            => 'newsletter',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Vertical Menu' , 'liftsupply'),
		'id'            => 'vertical-menu',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Left Sidebar' , 'liftsupply'),
		'id'            => 'sidebar-left',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Right Sidebar' , 'liftsupply'),
		'id'            => 'sidebar-right',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Blog Left Sidebar' , 'liftsupply'),
		'id'            => 'blog-sidebar-left',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Blog Right Sidebar', 'liftsupply'),
		'id'            => 'blog-sidebar-right',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Footer 1' , 'liftsupply'),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Footer 2' , 'liftsupply'),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Footer 3' , 'liftsupply'),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Footer 4' , 'liftsupply'),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Footer 5' , 'liftsupply'),
		'id'            => 'footer-5',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Mass Footer Body' , 'liftsupply'),
		'id'            => 'mass-footer-body',
		'description'   => esc_html__( 'Appears in the end of footer section of the site.', 'liftsupply'),
		'before_widget' => '<aside id="%1$s" class="widget-footer clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title hide"><span>',
		'after_title'   => '</span></h3>',
	));

	register_sidebar(
	array(
		'name'          => esc_html__( 'Custom Service', 'liftsupply'),
		'id'            => 'custom-service',
		'description'   => esc_html__( 'Appears in the header right section of the site.', 'liftsupply'),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Header support', 'liftsupply'),
		'id'            => 'header-support',
		'description'   => esc_html__( 'Appears in the header right section of the site.', 'liftsupply'),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	));
}
add_action( 'widgets_init', 'liftsupply_fnc_registart_widgets_sidebars' );

/**
 * Register Lato Google font for Liftsupply.
 *
 * @since Liftsupply 1.0
 *
 * @return string
 */
function liftsupply_fnc_font_url() {

	$fonts_url = '';

    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $lora = _x( 'on', 'Hind font: on or off', 'liftsupply' );

    /* Translators: If there are characters in your language that are not
    * supported by Open Sans, translate this to 'off'. Do not translate
    * into your own language.
    */
    $open_sans = _x( 'on', 'Open Sans font: on or off', 'liftsupply' );

 	$playfair = _x( 'on', 'Pay fair font: on or off', 'liftsupply' );

    if ( 'off' !== $lora || 'off' !== $open_sans || 'off' !==$playfair ) {
        $font_families = array();

        if ( 'off' !== $lora ) {
            $font_families[] = 'Poppins:400,300,500,600,700';
        }

        if ( 'off' !== $open_sans ) {
            $font_families[] = 'Arimo:400,700';
        }
 		if ( 'off' !== $playfair ) {
            $font_families[] = 'Playfair+Display:700,400italic';
        }
        $query_args = array(
            'family' => ( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );


 		$protocol = is_ssl() ? 'https:' : 'http:';
        $fonts_url = add_query_arg( $query_args, $protocol .'//fonts.googleapis.com/css' );
    }

    return esc_url_raw( $fonts_url );
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Liftsupply 1.0
 */
function liftsupply_fnc_scripts() {
	// Add Lato font, used in the main stylesheet.
	wp_enqueue_style( 'liftsupply-open-sans', liftsupply_fnc_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'liftsupply-fa', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '3.0.3' );

	if(isset($_GET['opal-skin']) && $_GET['opal-skin']) {
		$currentSkin = $_GET['opal-skin'];
	}else{
		$currentSkin = str_replace( '.css','', liftsupply_fnc_theme_options('skin','default') );
	}
	if( is_rtl() ){
		if( !empty($currentSkin) && $currentSkin != 'default' ){
			wp_enqueue_style( 'liftsupply-'.$currentSkin.'-style', get_template_directory_uri() . '/css/skins/rtl-'.$currentSkin.'/style.css' );
		}else {
			// Load our main stylesheet.
			wp_enqueue_style( 'liftsupply-style', get_template_directory_uri() . '/css/rtl-style.css' );
		}
	}
	else {
		if( !empty($currentSkin) && $currentSkin != 'default' ){
			wp_enqueue_style( 'liftsupply-'.$currentSkin.'-style', get_template_directory_uri() . '/css/skins/'.$currentSkin.'/style.css' );
		}else {
			// Load our main stylesheet.
			wp_enqueue_style( 'liftsupply-style', get_template_directory_uri() . '/css/style.css' );
		}
	}

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'liftsupply-ie', get_template_directory_uri() . '/css/ie.css', array( 'liftsupply-style' ), '20131205' );
	wp_style_add_data( 'liftsupply-ie', 'conditional', 'lt IE 9' );


	wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20130402' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}


	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.js', array( 'jquery' ), '20150315', true );
	wp_enqueue_script( 'prettyphoto-js',	get_template_directory_uri().'/js/jquery.prettyPhoto.js');
	wp_enqueue_style ( 'prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css');

	wp_enqueue_script ( 'liftsupply-functions-js', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150315', true );
	wp_localize_script( 'liftsupply-functions-js', 'liftsupplyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

}
add_action( 'wp_enqueue_scripts', 'liftsupply_fnc_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Liftsupply 1.0
 */
function liftsupply_fnc_admin_fonts() {
	wp_enqueue_style( 'liftsupply-lato', liftsupply_fnc_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'liftsupply_fnc_admin_fonts' );


/**
 * Implement rick meta box for post and page, custom post types. These 're used with metabox plugins
 */



require_once(  get_template_directory() . '/inc/classes/account.php' );
require_once(  get_template_directory() . '/inc/classes/nav.php' );
require_once(  get_template_directory() . '/inc/classes/offcanvas-menu.php' );

require_once(  get_template_directory() . '/inc/custom-header.php' );
require_once(  get_template_directory() . '/inc/customizer.php' );

require_once(  get_template_directory() . '/inc/function-post.php' );
require_once(  get_template_directory() . '/inc/function-unilty.php' );
require_once(  get_template_directory() . '/inc/functions-import.php' );
require_once(  get_template_directory() . '/inc/template-hook.php' );
require_once(  get_template_directory() . '/inc/template-tags.php' );
require_once(  get_template_directory() . '/inc/template.php' );


/**
 * Check and load to support visual composer
 */
if(  in_array( 'kingcomposer/kingcomposer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )  ){ 
	global $kc ;

	if( $kc ) {
		require_once(  get_template_directory() . '/inc/vendors/kingcomposer/kingcomposer.php' );
	}
}

/**
 * Check to support woocommerce
 */
if( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	add_theme_support( 'woocommerce');
	require_once(  get_template_directory() . '/inc/vendors/woocommerce/functions.php' );
}