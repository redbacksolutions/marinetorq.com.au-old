<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WpOpal
 * @subpackage Liftsupply
 * @since Liftsupply 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php liftsupply_fnc_post_thumbnail(); ?>

	<header class="entry-header blogv1">
		<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && liftsupply_fnc_categorized_blog() ) : ?>
 
		<?php
			endif;

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			endif;
		?>

		<div class="entry-meta">
			<?php
				if ( 'post' == get_post_type() )
					liftsupply_fnc_posted_on();

				if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
			?>
			<span class="comments-link"><span class="fa fa-comment-o"></span> <?php comments_popup_link( esc_html__( 'Leave a comment', 'liftsupply' ), esc_html__( '1 Comment', 'liftsupply' ), esc_html__( '% Comments', 'liftsupply' ) ); ?></span>
			<?php
				endif;

				edit_post_link( esc_html__( 'Edit', 'liftsupply' ), '<span class="edit-link">', '</span>' );
			?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content text-white">
		<?php
			/* translators: %s: Name of current post */
			echo liftsupply_fnc_excerpt( 25, '...' );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
