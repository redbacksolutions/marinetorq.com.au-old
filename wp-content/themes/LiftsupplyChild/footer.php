<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WpOpal
 * @subpackage Liftsupply
 * @since Liftsupply 1.0
 */
?>
		</section><!-- #main -->
		<?php do_action( 'liftsupply_template_main_after' ); ?>
		<?php do_action( 'liftsupply_template_footer_before' ); ?>

		<footer id="opal-footer" class="opal-footer" role="contentinfo">
			
			<div class="container">
				<div class="inner">
					
					<?php echo liftsupply_display_footer_content(); ?>
					<?php get_sidebar( 'mass-footer-body' );  ?>

				</div>
			</div>
  
			<section class="opal-copyright clearfix">
				<div class="container">
					<a href="#" class="scrollup"><span class="fa fa-angle-up"></span></a>
					<?php do_action( 'liftsupply_fnc_credits' ); ?>
					<?php  liftsupply_display_footer_copyright(); ?>
				</div>
			</section>
		</footer><!-- #colophon -->


		<?php do_action( 'liftsupply_template_footer_after' ); ?>
		<?php get_sidebar( 'offcanvas' );  ?>
	</div>
</div>
	<!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="//cdn.calltrk.com/companies/959333129/69f8a357c2fd5d1a001b/12/swap.js"></script>
</body>
</html>