<?php

function liftsupply_fnc_import_remote_demos() {
	return array(
		'liftsupply' => array(
			'name' 		=> 'liftsupply',
		 	'source'	=> 'http://wpsampledemo.com/liftsupply/liftsupply.zip',
		 	'preview'   => 'http://wpsampledemo.com/liftsupply/preview.jpg'
		),
	);
}

add_filter( 'wpopal_themer_import_remote_demos', 'liftsupply_fnc_import_remote_demos' );



function liftsupply_fnc_import_theme() {
	return 'liftsupply';
}
add_filter( 'wpopal_themer_import_theme', 'liftsupply_fnc_import_theme' );

function liftsupply_fnc_import_demos() {
	$folderes = glob( get_template_directory() .'/inc/import/*', GLOB_ONLYDIR );

	$output = array();

	foreach( $folderes as $folder ){
		$output[basename( $folder )] = basename( $folder );
	}

 	return $output;
}
add_filter( 'wpopal_themer_import_demos', 'liftsupply_fnc_import_demos' );

function liftsupply_fnc_import_types() {
	return array(
			'all' => 'All',
			'content' => 'Content',
			'widgets' => 'Widgets',
			'page_options' => 'Theme + Page Options',
			'menus' => 'Menus',
			'rev_slider' => 'Revolution Slider',
			'vc_templates' => 'VC Templates'
		);
}
add_filter( 'wpopal_themer_import_types', 'liftsupply_fnc_import_types' );


/**
 * Matching and resizing images with url.
 *
 *  $ouput = array(
 *        'allowed' => 1, // allow resize images via using GD Lib php to generate image
 *        'height'  => 900,
 *        'width'   => 800,
 *        'file_name' => 'blog_demo.jpg'
 *   );
 */
function liftsupply_import_attachment_image_size( $url ){

   $name = basename( $url );

   $ouput = array(
         'allowed' => 0
   );

   if( preg_match("#product#", $name) ) {
      $ouput = array(
         'allowed' => 1,
         'height'  => 1000,
         'width'   => 1000,
         'file_name' => 'product_demo.jpg'
      );
   }
   elseif( preg_match("#blog#", $name) || preg_match("#news-#", $name) || preg_match("#612YntpjodL#", $name) ){
      $ouput = array(
         'allowed' => 1,
         'height'  => 780,
         'width'   => 1170,
         'file_name' => 'blog_demo.jpg'
      );
   }
   elseif( preg_match("#portfolio#", $name) ){
      $ouput = array(
         'allowed' => 1,
         'height'  => 800,
         'width'   => 1170,
         'file_name' => 'portfolio_demo.png'
      );
   }

   return $ouput;
}

add_filter( 'pbrthemer_import_attachment_image_size', 'liftsupply_import_attachment_image_size' , 1 , 999 );
