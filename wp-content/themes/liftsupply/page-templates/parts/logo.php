<?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ): ?>
	<div id="opal-logo" class="logo">
    	<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<?php the_custom_logo(); ?>
		</a>
	</div>
<?php elseif( liftsupply_fnc_theme_options('logo') ):  ?>
	<div id="opal-logo" class="logo">
	    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
	        <img src="<?php echo liftsupply_fnc_theme_options('logo'); ?>" alt="<?php bloginfo( 'name' ); ?>">
	    </a>
	</div>
<?php else: ?>
    <div id="opal-logo" class="logo logo-transparent">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
             <img src="<?php echo get_template_directory_uri() . '/images/logo.png'; ?>" alt="<?php bloginfo( 'name' ); ?>" />
        </a>
    </div>
<?php endif; ?>