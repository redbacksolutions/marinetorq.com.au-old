<?php
$atts = array_merge( array(
		'title'  => '',
		'class'  => '',
		'images' => array(),
	), $atts);
extract( $atts );

wp_register_script('spritespin-rotate-js', get_template_directory_uri().'/js/spritespin.js', array( 'jquery' ), '20150315', true);
wp_enqueue_script('spritespin-rotate-js');


$_id = liftsupply_fnc_makeid();
$images_url = array();
// TH1

$product_id = 0;
if (isset($product_ids) && !empty($product_ids)) {

	$list = array_map( 'trim', explode( ',', $product_ids ) );
	$ids = explode(':', $list[0]);
	$product_id = get_page_by_path( $ids[0] ,OBJECT,'product');	
	if( $product_id ){
		$flag = true; 
	}
}

// TH2
$images = !empty($images) ? explode(',', $images) : array();

foreach ($images as $image_id) {
	$img_url = wp_get_attachment_url( $image_id );
	if ( !empty($img_url) ) {
		$images_url[] = esc_url($img_url);
	}
}


?>
<section class="widget kc-rotateimages <?php echo esc_attr($class); ?> <?php if($flag) { ?>col-lg-8 col-md-12 col-sm-12<?php } ?>">
    <?php if($title!=''){ ?>
        <h3 class="widget-title">
            <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>
    <div class="widget-content">
	    <div class="rotate">
	    	<div id="rotateimages-<?php echo esc_attr($_id); ?>">
			</div>
		</div>
    </div>
</section>
<?php if($flag) { ?>
<div class="product-style product-block product-info col-lg-4 col-md-12 col-sm-12">
<?php

$args = array(
   'post_type' => 'product',
   'post__in'  => array( $product_id->ID  ),
);
$loop = new WP_Query($args);

while ( $loop->have_posts() ) : $loop->the_post();
   $product = wc_get_product();
?>
    <h3 class="name">
	<a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>" title="<?php echo esc_attr( $product->get_name() ); ?>">
		<span class="product-title"><?php echo $product->get_name(); ?></span>
	</a>
	</h3>

    <div class="rating clearfix ">
    <?php if ( $rating_html = wc_get_rating_html( $product->get_average_rating() ) ) { ?>
        <div><?php echo trim( $rating_html ); ?></div>
    <?php }else{ ?>
        <div class="star-rating"></div>
    <?php } ?>
    </div>
    <div class="price"><?php echo trim( $product->get_price_html() ); ?></div>
    <div class="description"><?php echo liftsupply_fnc_excerpt(30,'...');; ?></div>
    <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
<?php endwhile; ?>
<?php wp_reset_query(); ?>
</div>
<?php } ?>

<?php if ( count($images_url) > 0 ):  
	wp_add_inline_script( 'spritespin-rotate-js', 
		'jQuery(document).ready(function($){
			$("#rotateimages-'.$_id.'").spritespin({
			  source: ['.'"'.implode('","', $images_url).'"'.'],
			  width   : 877,
			  height  : 550,
			});
		})' 
	);

endif; ?>