<?php
/**
 * Remove javascript and css files not use
 */
if( is_admin() ){
	function liftsupply_setup_admin_setting(){

		global $pagenow;
		if ( is_admin() && isset($_GET['activated'] ) && $pagenow == 'themes.php' ) {
			/**
			 *
			 */
			$pts = array( 'brands', 'footer', 'megamenu', 'portfolio', 'testimonials');

			$options = array();

			foreach( $pts as $key ){
				$options['enable_'.$key] = 'on';
			}

			update_option( 'wpopal_themer_posttype', $options );

			do_action( 'liftsupply_setup_theme_actived' );
		}
	}
	add_action( 'init', 'liftsupply_setup_admin_setting'  );
}


/**
 * Hoo to top bar layout
 */
function liftsupply_fnc_topbar_layout(){
	$layout = liftsupply_fnc_get_header_layout();
	get_template_part( 'page-templates/parts/topbar', $layout );
	get_template_part( 'page-templates/parts/topbar', 'mobile' );
}

add_action( 'liftsupply_template_header_before', 'liftsupply_fnc_topbar_layout' );

/**
 * Hook to select header layout for archive layout
 */
function liftsupply_fnc_get_header_layout( $layout='' ){
	global $post;

	$layout = $post && get_post_meta( $post->ID, 'liftsupply_header_layout', 1 ) ? get_post_meta( $post->ID, 'liftsupply_header_layout', 1 ) : liftsupply_fnc_theme_options( 'headerlayout' );

 	if( $layout == 'default' ){
 		return '';
 	}elseif( $layout){
 		return trim( $layout );
 	}elseif ( $layout = liftsupply_fnc_theme_options('header_skin','') ){
 		return trim( $layout );
 	}

	return $layout;
}

add_filter( 'liftsupply_fnc_get_header_layout', 'liftsupply_fnc_get_header_layout' );

/**
 * Hook to select header layout for archive layout
 */
function liftsupply_fnc_get_footer_profile( $profile='default' ){

	global $post;

	$profile =  $post? get_post_meta( $post->ID, 'liftsupply_footer_profile', 1 ):null ;
	
	$options = get_option( 'wpopal_themer_posttype' ); 
 	if( !isset($options['enable_footer']) || $options['enable_footer'] !='on' ){
 		return ;
 	}


 	if ( $profile && $profile != 'global' ) {
 		return trim( $profile );
 	} elseif ( $profile = liftsupply_fnc_theme_options('footer-style', $profile ) ) {
 		return trim( $profile );
 	}

	return $profile;
}

add_filter( 'liftsupply_fnc_get_footer_profile', 'liftsupply_fnc_get_footer_profile' );


/**
 * Hook to show breadscrumbs
 */
function liftsupply_fnc_render_breadcrumbs(){

	global $post;

	if( is_object($post) ){
		$disable = get_post_meta( $post->ID, 'liftsupply_disable_breadscrumb', 1 );
		if(  $disable || is_front_page() ){
			return true;
		}
		$bgimage = get_post_meta( $post->ID, 'liftsupply_image_breadscrumb', 1 );
		$color 	 = get_post_meta( $post->ID, 'liftsupply_color_breadscrumb', 1 );
		$bgcolor = get_post_meta( $post->ID, 'liftsupply_bgcolor_breadscrumb', 1 );
		$style = array();

		if( $bgcolor  ){
			$style[] = 'background-color:'.$bgcolor;
		}

		if( $bgimage  ){
			$style[] = 'background-image:url(\''.wp_get_attachment_url($bgimage).'\')';
		}else{
			$bgimage = liftsupply_fnc_theme_options( 'breadcrumb-bg' );
			$style[] =  $bgimage  ? 'background-image:url(\''.$bgimage.'\')' : "";
		}

		if( $color  ){
			$style[] = 'color:'.$color;
		}

		$estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
	} else {

		$bgimage = liftsupply_fnc_theme_options( 'breadcrumb-bg' );
		if( !empty($bgimage)  ){
			$style[] = 'background-image:url(\''.$bgimage.'\')';
		}
		$estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
	}

	echo '<section id="opal-breadscrumb" class="opal-breadscrumb" '.$estyle.'><div class="container">';
			liftsupply_fnc_breadcrumbs();
	echo '</div></section>';

}

add_action( 'liftsupply_template_main_before', 'liftsupply_fnc_render_breadcrumbs' );


/**
 * Main Container
 */

function liftsupply_template_main_container_class( $class ){
	global $post;
	global $liftsupply_wpopconfig;

	$layoutcls = get_post_meta( $post->ID, 'liftsupply_enable_fullwidth_layout', 1 );

	if( $layoutcls ) {
		$liftsupply_wpopconfig['layout'] = 'fullwidth';
		return 'container-fluid';
	}
	return $class;
}
add_filter( 'liftsupply_template_main_container_class', 'liftsupply_template_main_container_class', 1 , 1  );
add_filter( 'liftsupply_template_main_content_class', 'liftsupply_template_main_container_class', 1 , 1  );



/**
 * Get Configuration for Page Layout
 *
 */
function liftsupply_fnc_get_page_sidebar_configs( $configs='' ){

	global $post;

	$left  =  get_post_meta( $post->ID, 'liftsupply_leftsidebar', 1 );
	$right =  get_post_meta( $post->ID, 'liftsupply_rightsidebar', 1 );

	return liftsupply_fnc_get_layout_configs( $left, $right );
}

add_filter( 'liftsupply_fnc_get_page_sidebar_configs', 'liftsupply_fnc_get_page_sidebar_configs', 1, 1 );


function liftsupply_fnc_get_single_sidebar_configs( $configs='' ){

	global $post;

	$left  =  get_post_meta( $post->ID, 'liftsupply_leftsidebar', 1 );
	$right =  get_post_meta( $post->ID, 'liftsupply_rightsidebar', 1 );

	if ( empty( $left ) ) {
		$left  =  liftsupply_fnc_theme_options( 'blog-single-left-sidebar' );
	}

	if ( empty( $right ) ) {
		$right =  liftsupply_fnc_theme_options( 'blog-single-right-sidebar' );
	}

	return liftsupply_fnc_get_layout_configs( $left, $right );
}

add_filter( 'liftsupply_fnc_get_single_sidebar_configs', 'liftsupply_fnc_get_single_sidebar_configs', 1, 1 );

function liftsupply_fnc_get_archive_sidebar_configs( $configs='' ){

	global $post;


	$left  =  liftsupply_fnc_theme_options( 'blog-archive-left-sidebar' );
	$right =  liftsupply_fnc_theme_options( 'blog-archive-right-sidebar' );

	return liftsupply_fnc_get_layout_configs( $left, $right );
}

add_filter( 'liftsupply_fnc_get_archive_sidebar_configs', 'liftsupply_fnc_get_archive_sidebar_configs', 1, 1 );
add_filter( 'liftsupply_fnc_get_category_sidebar_configs', 'liftsupply_fnc_get_archive_sidebar_configs', 1, 1 );

/**
 *
 */
function liftsupply_fnc_get_layout_configs( $left, $right ){
	$configs = array();
	$configs['main'] = array( 'class' => 'col-lg-9 col-md-9' );

	$configs['sidebars'] = array(
		'left'  => array( 'sidebar' => $left, 'active' => is_active_sidebar( $left ), 'class' => 'col-lg-3 col-md-3 col-xs-12'  ),
		'right' => array( 'sidebar' => $right, 'active' => is_active_sidebar( $right ), 'class' => 'col-lg-3 col-md-3 col-xs-12' )
	);

	if( $left && $right ){
		$configs['main'] = array( 'class' => 'col-lg-6 col-md-6' );
	} elseif( empty($left) && empty($right) ){
		$configs['main'] = array( 'class' => 'col-lg-12 col-md-12' );
	}
	return $configs;
}


function liftsupply_fnc_sidebars_others_configs(){

	global $liftsupply_page_layouts;

	return $liftsupply_page_layouts;
}

add_filter("liftsupply_fnc_sidebars_others_configs", "liftsupply_fnc_sidebars_others_configs");