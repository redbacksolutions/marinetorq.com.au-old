Version: 1.3 Updated 26-October-2017
- update woo 3.2
- remove file js
- update file languages
- update menu mobile

Change log:
/themes/liftsupply/js/functions.js
/themes/liftsupply/languages/liftsupply.pot
/themes/liftsupply/style.css
/themes/liftsupply/woocommerce/cart/mini-cart.php
/themes/liftsupply/woocommerce/checkout/form-shipping.php
/themes/liftsupply/woocommerce/single-product-reviews.php
(-) /themes/liftsupply/js/customizer.js
(-) /themes/liftsupply/js/featured-content-admin.js
(-) /themes/liftsupply/js/isotope.pkgd.min.js
(-) /themes/liftsupply/js/jquery.counterup.min.js
(-) /themes/liftsupply/js/slider.js
(-) /themes/liftsupply/js/waypoints.min.js